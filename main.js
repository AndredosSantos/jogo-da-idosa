const main = document.getElementById("main");

const map = [
  ["", "", ""],
  ["", "", ""],
  ["", "", ""],
];

let player = true;

const verifyPlayerOneWin = (currentValue) => currentValue === "X";
const verifyPlayerTwoWin = (currentValue) => currentValue === "O";
const verifyDraw = (currentValue) => currentValue !== "";

const verifyAllChances = (verifyPlayer) => {
  const allChances = [
    [map[0][0], map[1][1], map[2][2]],
    [map[0][2], map[1][1], map[2][0]],
    map[0],
    map[1],
    map[2],
    [map[0][0], map[1][0], map[2][0]],
    [map[0][1], map[1][1], map[2][1]],
    [map[0][2], map[1][2], map[2][2]],
  ];

  const win = [];

  for (let index = 0; index < allChances.length; index++) {
    const currentChance = allChances[index];
    win.push(currentChance.every(verifyPlayer));
  }

  return win.includes(true);
};

const handleVerifyWin = (player) => {
  if (player && verifyAllChances(verifyPlayerOneWin)) {
    alert("Jogador 1 vencey");
    main.removeEventListener("click", handleOnClickFunction);
  } else if (player === false && verifyAllChances(verifyPlayerTwoWin)) {
    alert("Jogador 2 vencey");
    main.removeEventListener("click", handleOnClickFunction);
  }
};

const handleOnClickFunction = (e) => {
  const target = e.target;

  const line = Number(target.getAttribute("line"));
  const column = Number(target.getAttribute("column"));

  if (player && map[line][column] === "") {
    map[line][column] = "X";
    handleVerifyWin(player);
    player = !player;
  } else if (map[line][column] === "") {
    map[line][column] = "O";
    handleVerifyWin(player);
    player = !player;
  }

  handleBuildMap(map);
};

main.addEventListener("click", handleOnClickFunction);

const handleBuildLine = (currentLine, line) => {
  for (let column = 0; column < currentLine.length; column++) {
    const currentValue = currentLine[column];

    const div = document.createElement("div");

    div.setAttribute("column", column);
    div.setAttribute("line", line);

    div.innerText = currentValue;

    main.appendChild(div);
  }
};

const handleBuildMap = (map) => {
  main.innerHTML = "";

  for (let line = 0; line < map.length; line++) {
    const allLineItens = map[line];
    handleBuildLine(allLineItens, line);
  }
};

handleBuildMap(map);
